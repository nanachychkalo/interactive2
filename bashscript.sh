#!/bin/bash

export MY_CUSTOM_VARIABLE2="HiggsDiscovery"

my_variable="X"
my_array=("H" "W" "Z")

for element in "${my_array[@]}"; do
    echo $element
done

if [[ $my_variable == "X" ]]; then
    echo "Our condition is true!"
fi

printenv | grep MY_CUSTOM_VARIABLE
